<?php
/**
 * appSync unit tests for sync between clients and server
 * version: 0.1
 * date: 18/07/2014
 *
 */

include("appsync.php");
$conflictHandling = TIMESTAMPPRIORITY;

$server = new Server("server");
$client1 = new Client("client1", $server);
$client2 = new Client("client2", $server);

echo "*** START Creating cherries with PK 2014-06-20 on client 1\n";
$client1->addObject("2014-06-20", "cherries", "1");
echo "*** END   Creating cherries on client 1\n";
$client1->display();

sleep(2); //make sure objects have different timestamps_lastupdate on client 1 and client 2

echo "*** START Creating cherries with PK 2014-06-20 on client 2\n";
$client2->addObject("2014-06-20", "cherries", "2");
echo "*** END   Creating cherries on client 2\n";
$client2->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Do sync on client 2\n";
$client2->doSync();
echo "*** END   Do sync on client 2\n";
$client2->display();
$server->display();

echo "*** START Sync again on client 1 \n";
$client1->doSync();
echo "*** END   Sync again on client 1 \n";
$client1->display();
$server->display();

echo "*** START Sync again on client 2 (no data should be synced) \n";
$client2->doSync();
echo "*** END   Sync again on client 2 (no data should be synced) \n";
$client2->display();
$server->display();

echo "*** START Sync again on client 1 (no data should be synced) \n";
$client1->doSync();
echo "*** END   Sync again on client 1 (no data should be synced) \n";
$client1->display();
$server->display();